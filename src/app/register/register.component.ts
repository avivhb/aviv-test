import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email:string;
  password:string;
  errorMessage:string;
  isError:boolean = false;
  
  onSubmit(){
    this.Auth.register(this.email,this.password).then(res =>{
      this.router.navigate(['/welcome']);
      }).catch(
        err => {
          this.isError = true;
          this.errorMessage = err.message;
        }
            
        )
  }

  constructor(private Auth:AuthService,private router:Router) { }

  ngOnInit(): void {
  }

}
