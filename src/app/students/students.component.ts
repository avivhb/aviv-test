import { StudentService } from './../student.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  students$;
  students:any[];
  userId:string;

  displayedColumns: string[] = ['userName', 'name', 'psychometric', 'mathematics', 'predict', 'delete'];



  delete(studentId){
    this.studentService.deleteCustomer(studentId);
  }



  constructor(private studentService:StudentService,public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          this.students$ = this.studentService.getStudents();
          this.students$.subscribe(
            docs => {         
              this.students = [];
              for (let document of docs) {
                const students:any = document.payload.doc.data();
                students.id = document.payload.doc.id;
                this.students.push(students); 
              }                        
            }
          )
      })
 }
  }