import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  customersCollection:AngularFirestoreCollection;
  studentCollection:AngularFirestoreCollection = this.db.collection('students');

  addStudent(userName:string,name:string,psychometric:number,mathematics:number,predict:string){
    const student = {userName:userName,name:name,psychometric:psychometric,mathematics:mathematics,predict:predict};
    this.db.collection(`students`).add(student);
  }

  
  getStudents(): Observable<any[]> {
    this.customersCollection = this.db.collection(`students`);
    return this.customersCollection.snapshotChanges();    
  }

  deleteCustomer(studentId:string){
    this.db.doc(`students/${studentId}`).delete();
  }

  constructor(private db:AngularFirestore,) { }
}
