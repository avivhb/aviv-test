import { StudentService } from './../student.service';
import { Component, OnInit } from '@angular/core';
import { LambdaService } from '../lambda.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {

  choises: any[] = [
    {value: 'paid', viewValue: 'paid'},
    {value: 'Not paid', viewValue: 'Not paid'}
  ];

  name:string;
  mathematics:number;
  psychometric:number;
  selectedValue;

  predict:string;

  userId:string;
  userMail:string;

  sendData(){
    if(this.selectedValue == "Not paid"){
      this.selectedValue =0;
    }else{
      this.selectedValue =1;
    }
    this.lambdaService.predict(this.selectedValue,this.psychometric,this.mathematics).subscribe(
      res =>  {console.log(res);
         if (res > 0.5){
           var result = 'success';
         }else{
           var result = 'drop';
         }
         this.predict = result;
      }
      );
    }

    cancelled(){
      this.predict = null;
    }

    // addStudent(userName:string,name:string,psychometric:number,mathematics:number){
    add(){
      this.studentService.addStudent(this.userMail,this.name,this.psychometric,this.mathematics,this.predict);
      this.router.navigate(['/students']);
    }
 
  constructor(private lambdaService:LambdaService, private studentService:StudentService,public authService:AuthService,private router:Router) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
            user => {
              this.userId = user.uid;
              this.userMail = user.email; 
                }
              )
            }
  }