import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  errorMessage:string;
  isError:boolean = false;

  onSubmit(){
    this.Auth.login(this.email,this.password).then(res =>{
      this.router.navigate(['/welcome']);
      }).catch(
        err => {
          this.isError = true;
          this.errorMessage = err.message;
        })
  }


  constructor(private Auth:AuthService, private router:Router) { }

  ngOnInit(): void {
  }

}
