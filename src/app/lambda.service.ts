import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LambdaService {

  private url = 'https://up1dyio0k3.execute-api.us-east-1.amazonaws.com/27-01-2021';

  predict(paid,psychometric,mathematics):Observable<any>{
    let json = {
      "data": 
        {
          "paid": paid,
          "psychometric": psychometric,
          "mathematics": mathematics
        }
    }
    let body  = JSON.stringify(json);
    console.log(body);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

  constructor(private http:HttpClient) { }
}
