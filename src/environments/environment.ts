// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyA9EZH4k3-pH9xZIXLNd_BElyJ3LVmiZwM",
    authDomain: "test-f6414.firebaseapp.com",
    projectId: "test-f6414",
    storageBucket: "test-f6414.appspot.com",
    messagingSenderId: "23727227516",
    appId: "1:23727227516:web:12ac6913a43ef129e6bff9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
